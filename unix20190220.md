# unix 20190220 筆記

# 一 argument
在執行code前，先用以下指令安裝 C++ 的編譯環境

``` bash
    sudo apt install build-essential -y
```
就可以開始使用

這是 `args.c` 的 `code`

```C
#include <stdio.h>
int main(int argc, char *argv[]) {
    int i;
    for(i = 0; i < argc; i++) {
        printf("'%s' ", argv[i]);
    }
    printf("\n");
    return 0;
}
```

在使用之前，請先進行編譯
```bash
    gcc -o args args.c
```
在用以下方法進行
```bash
    ./args "a b c d e $HOME"
```
其中 `./` 是指當前目錄下

argc 為參數個數，全名是 argument count; 所以共有 7 個參數（因為涵蓋 `./args` ） 

這邊是用雙引號，因此 shell 會幫你把它直譯
`$HOME`變數，今天如果用單引號，會原封不動的直接印出`$HOME` 字串而不會去解讀它 。

再來，如果輸入
```bash
./args ../*
```
輸出

    './args' '../advio' '../advipc' '../asm' '../classipc' '../daemon' '../env+tools' '../files' '../ipc' '../Makefile' '../netipc' '../procctl' '../procenv' '../procrel' '../signal' '../sysinfo' '../termio' '../threads'

`../` : 父目錄  ＋ `*` : 所有檔案
所以shell 會幫你解譯這個參數，這邊就跟 windows 不太一樣，參數是由shell解讀而不是由program本身

最後試試看

```bash
./args *
```
輸出`./args` 和當前目錄所有檔案
'./args' 'args' 'args.c' 'bug2.c' 'bug.c' 'errno.c' 'gdbcall.c' 'getopt.c' 'hello.c' 'make' 'Makefile' 'return.c' 'rusage.c' 'testret.sh' 'testtime.c'

# 二 getopt
> getopt 是一個 API，可用來parse command line 的參數，其中 ping 指令就有使用到這個

[參考網址1](http://carl830.pixnet.net/blog/post/50806433-c%E8%AA%9E%E8%A8%80-getopt%E7%94%A8%E6%B3%9)

[參考網址2](http://wen00072.github.io/blog/2013/12/29/using-getopt-parse-command-line-parameter/)

# 三 計時指令(可以測試程式效能)
例如
```bash
time sleep 5
```
五秒鐘之後會回傳

    real	0m5.002s
    user	0m0.001s
    sys	0m0.000s

real : 手錶真正的時間
user ： user space 執行的時間
sys  ： kernel space 執行的時間

因此 user space 先傳指令去 kernel space 執行 sleep，這裡有 0 是因為 精確度不夠，而且這兩個 user 和 sys 是 CPU time 而不是
real time，取決於 CPU 的 cycle 。

