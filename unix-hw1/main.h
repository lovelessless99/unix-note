
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>
#include <dirent.h>
#include <unistd.h>
#define  TEMP_BUFFER_SIZE 0x10000

struct information {
    char protocal[5];
    unsigned int local_address_ip;
    unsigned int rem_address_ip;
    unsigned int local_address_port;
    unsigned int rem_address_port; 
    unsigned int line_id;
    unsigned int conn_state;
    unsigned int inode;
};


struct process_information{
    char pid_with_filename[100];
    char filename[100];
    int inode[2000];
    int size;
};

void print_connection(char* protocal, char* filter);
char* convert_address(unsigned int, int);
struct process_information read_status(long int pid);
int get_all_process_state(struct process_information []);

char* convert_address(unsigned int ip, int version){
    char str[100];
    inet_ntop((version == 4)? AF_INET:AF_INET6, &ip, str, sizeof str);
    return strdup(str);
}


int get_all_process_state(struct process_information info[]){
    DIR* proc = opendir("/proc/");
    struct dirent* ent;
    long tgid;

    if(proc == NULL) {
        perror("opendir(/proc)");
        return 0;
    }
    int k = 0;
    while(ent = readdir(proc)) {
        if(!isdigit(*ent->d_name))
            continue;
        
        tgid = strtol(ent->d_name, NULL, 10);
        info[k++] = read_status(tgid);
    }
    
    closedir(proc);
    return k;
}


struct process_information read_status(long int pid){
    struct process_information all;
    

    char str[64];
    char destination[100] = "/proc/";
    char process_stat[100];
    sprintf(str, "%ld", pid);
    strcat(destination, str);
    strcpy(process_stat, destination);


    strcat(destination, "/fd/");
    strcat(process_stat, "/stat");


    DIR* proc = opendir(destination);
    
    struct dirent* ent;
    
    if(proc != NULL){
        
        // visit 所有 file descriptor
        while(ent = readdir(proc)) {
            if(!isdigit(*ent->d_name))
                continue;

            char fd_path[100];
            strcpy(fd_path, destination);
            strcat(fd_path, ent->d_name);
            
            char buf[512];
            
            int count = readlink(fd_path, buf, sizeof(buf));
            if (count >= 0) {
                buf[count] = '\0';
                if(strstr(buf, "socket") != NULL){
                    //printf("%s -> %s\n", fd_path, buf);
                    int inode_num;
                    sscanf(buf," socket:[%d]", &inode_num);
                    //printf("%d\n", inode_num);
                    //printf("%d ", all.size);
                    all.inode[all.size++] = inode_num;
                }
            }
        }

        FILE *fp = fopen(process_stat  , "r" );

        if(fp != NULL){
            char line_buffer[ TEMP_BUFFER_SIZE ];
            char filename[1000];
            int pid_;
            
            while ( fgets( line_buffer, TEMP_BUFFER_SIZE-1, fp) )
            {    
                sscanf(line_buffer,"%d  (%s) ", &pid_, filename);
            }
            filename[strlen(filename)-1] = '\0';
            
            strcpy(all.filename, filename);
            strcat(str, "/ ");
            strcat(str, filename);
            strcpy(all.pid_with_filename, str);


        }

    }

    closedir(proc);
    return all;
    
}

void help(){
    printf("Usage: ./hw1 [ OPTIONS ]\n");
    printf("       ./hw1 [ OPTIONS ] [ FILTER ]\n");
    printf("  -t, --tcp              List TCP connection\n");
    printf("  -u, --udp              List UDP connection\n");
    printf("  -h, --help             this essage\n\n");

}