# include "main.h"
const char* short_options = "tuh";
const struct option long_options[]= {
    { "tcp", 0, NULL, 't' },
    { "udp", 0, NULL, 'u' },
    { "help", 0, NULL, 'h'}
};

int main(int argc, char *argv[])
{
    int c;

    if(argc == 1){
        print_connection("tcp", NULL);
        print_connection("udp", NULL);
        return 0;
    }
    
    
    while( (c = getopt_long(argc, argv, short_options, long_options, NULL)) != -1){
        switch(c){
            case 't': 
                print_connection("tcp", ( argc == 2  )? NULL : argv[argc-1]);
                break;

            case 'u': 
                print_connection("udp", ( argc == 2 )? NULL : argv[argc-1]);
                break;

            case 'h':
                help();
                break;
        
            default:
                help();
        }
    }
        
    return 0;
}

void print_connection(char* protocal, char* filter){
    char line_buffer[ TEMP_BUFFER_SIZE ];
    char protocal_directory[12] = "/proc/net/";
    struct information info;
    FILE *fp[2];

    if(strstr(protocal, "tcp") != NULL){
        printf("List of TCP connections:\n");
    }
    else if(strstr(protocal, "udp") != NULL){  
        printf("List of UDP connections:\n");
       
    }
    
    strcpy(info.protocal, protocal); 
    strcat(protocal_directory, protocal);
    fp[0] = fopen(protocal_directory  , "r" );
    
    strcat(protocal_directory, "6");
    fp[1] = fopen(protocal_directory  , "r" );
    

    printf("Proto                        Local Address                              Foreign Address         PID/Program name and arguments \n");
    
    struct process_information p[500] ;
    int size = get_all_process_state(p);
    int first = 1;
    for(int i = 0 ; i < 2 ; i++, strcat(info.protocal, "6")){
        while ( fgets( line_buffer, TEMP_BUFFER_SIZE-1, fp[i] ) )
        {   
            unsigned int _;
                
            int uid, timeout;
            sscanf(line_buffer,"%d: %X:%X %X:%X %X",
                    &info.line_id,
                    &info.local_address_ip,
                    &info.local_address_port,
                    &info.rem_address_ip,
                    &info.rem_address_port,
                    &info.conn_state
            );

            if(first == 1) {first = 0 ; continue;}
           
            if(filter == NULL){
                
                if( i == 0 )
                    printf("%s       %26s:%05d             %26s:%05d          ",
                            info.protocal, convert_address(info.local_address_ip, 4),  info.local_address_port,   convert_address(info.rem_address_ip, 4)
                            , info.rem_address_port);
                
                else 
                    printf("%s       %25s:%05d             %26s:%05d         ",
                            info.protocal, convert_address(info.local_address_ip, 6),  info.local_address_port,   convert_address(info.rem_address_ip, 6)
                            , info.rem_address_port);
               
                sscanf(line_buffer,"%d: %X:%X %X:%X %X %X:%X %X:%X %X  %d  %d %d", &_, &_, &_, &_, &_, &_, &_, &_, &_, &_, &_, &_, &_, &info.inode);
                
            
                int find = 0;
                for(int k = 0; k < size && !find; k++){
                    for(int h = 0 ; h < p[k].size ; h++){
                        if(info.inode == p[k].inode[h]){

                            if(i == 0)printf("%20s", p[k].pid_with_filename);
                            else printf("%21s", p[k].pid_with_filename);

                            find = 1;
                            break;
                        }
                    }
                    
                }

                printf("\n");
            }

            else{
                
                sscanf(line_buffer,"%d: %X:%X %X:%X %X %X:%X %X:%X %X  %d  %d %d", &_, &_, &_, &_, &_, &_, &_, &_, &_, &_, &_, &_, &_, &info.inode);
                
                // 相同 inode 且和參數字串一樣
                int find = 0;
                for(int k = 0; k < size && !find; k++){
                    for(int h = 0 ; h < p[k].size ; h++){
                        if(info.inode == p[k].inode[h] && !strcmp(filter, p[k].filename)){
                            //  
                            //printf("%s %s\n", filter, p[k].filename);

                            sscanf(line_buffer,"%d: %X:%X %X:%X %X",
                                    &info.line_id,
                                    &info.local_address_ip,
                                    &info.local_address_port,
                                    &info.rem_address_ip,
                                    &info.rem_address_port,
                                    &info.conn_state
                            );

                            if(i == 0){
                                printf("%s       %26s:%05d             %26s:%05d          ",
                                info.protocal, convert_address(info.local_address_ip, 4),  info.local_address_port,   convert_address(info.rem_address_ip, 4), info.rem_address_port);
                                printf("%20s\n", p[k].pid_with_filename);
                            }
                            else {
                                printf("%s       %25s:%05d             %26s:%05d         ",
                                info.protocal, convert_address(info.local_address_ip, 6),  info.local_address_port,   convert_address(info.rem_address_ip, 6)
                                , info.rem_address_port);
                                printf("%21s\n", p[k].pid_with_filename);
                            
                            }
                            find = 1;
                            break;
                        }
                    }
                    
                }


            }
        }
    }
    printf("\n");
    
}
