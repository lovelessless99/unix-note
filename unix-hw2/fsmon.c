#include <dlfcn.h>
#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
// #include <sys/stat.h>

#include <fcntl.h>
#include <sys/types.h>

static DIR *(*funcp)(const char *name) = NULL;
static struct dirent *(*rdir)(DIR *) = NULL;
static int (*cdir)(DIR *) = NULL;

static int (*open_file)(const char *path, int oflag, ... ) = NULL;
static ssize_t (*read_file)(int fd, void * buf, size_t count) = NULL;
static ssize_t (*write_file)(int fd, const void *buf, size_t count) = NULL;
static int (*close_file)(int fd) = NULL;

static FILE* (*fopen_file)(const char*, const char*) = NULL;
static int (*fclose_file)( FILE * stream ) = NULL;
static size_t(*fread_file)(void *ptr, size_t size, size_t nmemb, FILE *stream) = NULL;
static size_t(*fwrite_file)(const void *ptr, size_t size, size_t nmemb, FILE *stream) = NULL;

static int(*fget_file)(FILE * stream ) = NULL;
static char *(*fgets_file)(char *s, int size, FILE *stream) = NULL;
static int (*fscanf_file)(FILE *stream, const char *format, ...) = NULL;
static int (*fprintf_file)(FILE *stream, const char *format, ...) = NULL;
static int (*fflush_file)(FILE *stream) = NULL;

static int (*chdir_dir)(const char * path) = NULL;
static int (*chown_dir)(const char *pathname, uid_t owner, gid_t group) = NULL;
static int (*chmod_dir)(const char *pathname, mode_t mode) = NULL;
static int (*remove_file)(const char *filename) = NULL;
static int (*rename_file)(const char *old_filename, const char *new_filename) = NULL;

static int (*mkdir_dir)(const char *path, mode_t mode) = NULL;
static int (*rmdir_dir)(const char *path) = NULL;

static int (*link_file)(const char *path1, const char *path2) = NULL;
static int (*unlink_file)(const char *pathname) = NULL;
static ssize_t (*readlink_file)(const char *pathname, char *buf, size_t bufsiz) = NULL;
static int (*symlink_file)(const char *path1, const char *path2) = NULL;

static int (*stat_file)(const char *path, struct stat *buf) = NULL;
static int (*lstat_file)(const char *path, struct stat *buf) = NULL;

static int (*dup_file)(int oldfd) = NULL;
static int (*dup2_file)(int oldfd, int newfd) = NULL;

static int (*creat_file)(const char *path, mode_t mode) = NULL;
static ssize_t (*pwrite_file)(int fd, const void *buf, size_t count, off_t offset) = NULL;

__attribute__((constructor)) static void beforeFunction(int argc, const char **argv, const char **envp)
{
		
		char *search_prefix = strstr(*++envp, "MONITOR_OUTPUT=");
		char target_file_name[20];
		int prefix_size = strlen("MONITOR_OUTPUT=");
		if(search_prefix != NULL){
			strncpy(target_file_name, *envp+prefix_size, 20);
			fprintf(stderr ,"------have monitor variable-------%s\n", target_file_name);
			int fd = open(target_file_name, O_RDWR | O_CREAT | O_TRUNC, 00644);
			//int fd = open(target_file_name, 0644);
			dup2(fd, 2);
		}
}


struct directory{
	DIR* dir;
	const char *name;
};

struct directory current_dir;

DIR* opendir(const char *name) {
	if(funcp == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			funcp = dlsym(handle, "opendir");
		}
	}

	DIR * d = funcp(name);
	current_dir.dir = d;
	current_dir.name = name;

	if(funcp != NULL) {
		fprintf(stderr, "opendir(%s) = %p\n", name, d);
	}
	return d;
}


struct dirent * readdir(DIR * dirp) {
	
	if(rdir == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			rdir = dlsym(handle, "readdir");
		}
	}
	
	struct dirent *d;
	if( (d = rdir(dirp)) != NULL) {
		if(dirp == current_dir.dir)
			fprintf(stderr, "readdir(%s) = %s\n" ,current_dir.name, d->d_name);
		else
			fprintf(stderr, "readdir = %s\n" , d->d_name);
	}
	return d;
}

int closedir(DIR *dirp){
	if(cdir == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			cdir = dlsym(handle, "closedir");
		}
	}
	
	int d = cdir(dirp);
	if(cdir != NULL) {
		if(dirp == current_dir.dir)
			fprintf(stderr, "closedir(%s) = %d\n" ,current_dir.name, d);
		else
			fprintf(stderr, "closedir(%p) = %d\n" ,dirp , d);
	}
	return d;
}

struct file_with_descriptor{
	const char *path;
	int fd;
};

struct file_with_descriptor file_with_fd;

int open(const char *path, int oflag, ... ){
    if(open_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			open_file = dlsym(handle, "open");
		}
	}
	va_list ar;
	va_start(ar, oflag);
	int mode = va_arg(ar,int);
  int d = open_file(path, oflag, mode );
	file_with_fd.path = path;
	file_with_fd.fd = d;
  
	if(open_file != NULL) {
		fprintf(stderr, "open(%s, %x, %d) = %d\n", path, oflag, mode, d);
	}
	va_end(ar);
	return d;
}

ssize_t read(int fd, void * buf, size_t count){
	if(read_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			read_file = dlsym(handle, "read");
		}
	}

    ssize_t d = read_file(fd, buf, count);
    if( read_file != NULL) {
			if(file_with_fd.fd == fd)
				fprintf(stderr, "read(%s, %p, %ld) = %ld\n", file_with_fd.path, buf, count, d);
			else
				fprintf(stderr, "read(%d, %p, %ld) = %ld\n", fd, buf, count, d);
	
	}
	return d;
}

ssize_t write(int fd, const void *buf, size_t count){
	if(write_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			write_file = dlsym(handle, "write");
		}
	}

    ssize_t d = write_file(fd, buf, count);
    if( write_file != NULL) {
		fprintf(stderr, "write(%d, %p, %ld) = %ld\n", fd, buf, count, d);
	}
	return d;
}

int close(int fd){
	if(close_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			close_file = dlsym(handle, "close");
		}
	}

    int d = close_file(fd);
    if( close_file != NULL) {
		fprintf(stderr, "close(%d) = %d\n", fd, d);
	}
	return d;
}

FILE* fopen(const char* filename, const char* mode){
	if(fopen_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			fopen_file = dlsym(handle, "fopen");
		}
	}

    FILE* d = fopen_file(filename, mode);
    if( fopen_file != NULL) {
		fprintf(stderr, "fopen(%s, %s) = %p\n", filename, mode, d);
	}
	return d;
}

int fclose ( FILE * stream ){
	if(fclose_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			fclose_file = dlsym(handle, "fclose");
		}
	} 

    int d = fclose_file(stream);
    if( fclose_file != NULL) {
		fprintf(stderr, "fclose(%p) = %d\n", stream, d);
	}
	return d;
}

size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream){
	if(fread_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			fread_file = dlsym(handle, "fread");
		}
	} 

    size_t d = fread_file(ptr, size, nmemb, stream);
    if( fread_file != NULL) {
		fprintf(stderr, "fread(%p, %ld, %ld, %p) = %ld\n", ptr, size, nmemb, stream, d);
	}
	return d;

}

size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream){
	if(fwrite_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			fwrite_file = dlsym(handle, "fwrite");
		}
	} 

    size_t d = fwrite_file(ptr, size, nmemb, stream);
    if( fwrite_file != NULL) {
		printf("fwrite(%p, %ld, %ld, %p) = %ld\n", ptr, size, nmemb, stream, d);
	}
	return d;
}

int fgetc(FILE *stream){
	if(fget_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			fget_file = dlsym(handle, "fgetc");
		}
	} 

    int d = fget_file(stream);
    if( fget_file != NULL) {
		fprintf(stderr, "fgetc(%p) = %d\n", stream, d);
	}
	return d;
}


char *fgets(char *s, int size, FILE *stream){
	if(fgets_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			fgets_file = dlsym(handle, "fgets");
		}
	} 
	char *temp = s;
	temp[strlen(temp)-1]='\0';

	char *get_string  = fgets_file(temp, size, stream);
  

  if( fgets_file != NULL) {
		if(get_string != NULL){
			fprintf(stderr, "fgets(%s, %d, %p) = %s", temp, size, stream, get_string);
		}
		else{
			fprintf(stderr, "fgets(%s, %d, %p) = 0\n", temp, size, stream);
		}
		
	}
	return get_string;
}

int fscanf(FILE *stream, const char *format, ...){
	if(fscanf_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			fscanf_file = dlsym(handle, "fscanf");
		}
	} 
		
	va_list ap;
	va_start(ap, format);
  int d = fscanf_file(stream, format, va_arg(ap, char*));
    if( fscanf_file != NULL) {
		printf("fscanf(%p, %s) = %d\n", stream, format, d);
	}
	// fclose(stderr);
	va_end(ap);
	return d;
}

int fflush(FILE *stream){
	if(fflush_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			fflush_file = dlsym(handle, "fflush");
		}
	} 
	
  int d = fflush_file(stream);
    if( fflush_file != NULL) {
			fprintf(stdout, "fflush(%p) = %d (%x)\n", stream, d, d);
		}
	return d;	
}

int _fprintf(FILE *stream, const char *format, ...){
	if(fprintf_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			fprintf_file = dlsym(handle, "fprintf");
		}
	}

	va_list ap;
	va_start(ap, format);

	int d = fprintf_file(stream, format, va_arg(ap, char*));
	
	if( fprintf_file != NULL) {
		fprintf_file(stderr, "fprintf(%s) = %d\n", format, d);
		//fprintf(stderr, "fprintf(%p, %s) = %d\n", stream, format, d);
	}
	//fclose(stderr);
	va_end(ap);
	return d;
}

int chdir(const char * path){
	if(chdir_dir == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			chdir_dir = dlsym(handle, "chdir");
		}
	} 

    int d = chdir_dir(path);
    if( chdir_dir != NULL) {
		fprintf(stderr, "chdir(%s) = %d\n", path, d);
	}
	return d;
}

int chown(const char *pathname, uid_t owner, gid_t group){
	if(chown_dir == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			chown_dir = dlsym(handle, "chown");
		}
	} 

  int d = chown_dir(pathname, owner, group);
    if( chown_dir != NULL) {
			fprintf(stderr, "chown(%s) = %d\n", pathname, d);
	}
	return d;

}

int chmod(const char *pathname, mode_t mode){
	if(chmod_dir == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			chmod_dir = dlsym(handle, "chmod");
		}
	} 

    int d = chmod_dir(pathname, mode);
    if( chmod_dir != NULL) {
		fprintf(stderr, "chmod(%s) = %d\n", pathname, d);
	}
	return d;
}

int remove(const char *filename){

	if(remove_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			remove_file = dlsym(handle, "remove");
		}
	} 

    int d = remove_file(filename);
    if( remove_file != NULL) {
		fprintf(stderr, "remove(%s) = %d\n", filename, d);
	}
	return d;
}

int rename(const char *old_filename, const char *new_filename){
	if(rename_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			rename_file = dlsym(handle, "rename");
		}
	} 

    int d = rename_file(old_filename, new_filename);
    if( rename_file != NULL) {
		fprintf(stderr, "rename(%s, %s) = %d\n", old_filename, new_filename, d);
	}
	return d;
}

int mkdir(const char *path, mode_t mode){
	if(mkdir_dir == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			mkdir_dir = dlsym(handle, "mkdir");
		}
	} 

    int d = mkdir_dir(path, mode);
    if( mkdir_dir != NULL) {
		fprintf(stderr, "mkdir(%s, %3o) = %d\n", path, mode&0777, d);
	}
	return d;
}

int rmdir(const char *path){
	if(rmdir_dir == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			rmdir_dir = dlsym(handle, "rmdir");
		}
	} 

    int d = rmdir_dir(path);
    if( rmdir_dir != NULL) {
		fprintf(stderr, "rmdir(%s) = %d\n", path, d);
	}
	return d;
}

int link(const char *path1, const char *path2){
	if(link_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			link_file = dlsym(handle, "link");
		}
	} 

    int d = link_file(path1, path2);
    if( link_file != NULL) {
		fprintf(stderr, "link(%s, %s) = %d\n", path1, path2, d);
	}
	return d;
}

int unlink(const char *pathname){
	if(unlink_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			unlink_file = dlsym(handle, "unlink");
		}
	} 

    int d = unlink_file(pathname);
    if( unlink_file != NULL) {
		fprintf(stderr, "unlink(%s) = %d\n", pathname, d);
	}
	return d;
}

ssize_t readlink(const char *pathname, char *buf, size_t bufsiz){
	
	if(readlink_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			readlink_file = dlsym(handle, "readlink");
		}
	}

    ssize_t d = readlink_file(pathname, buf, bufsiz);
    if( readlink_file != NULL) {
		fprintf(stderr, "readlink(%s, %p, %ld) = %ld\n", pathname, buf, bufsiz, d);
	}
	return d;
}

int symlink(const char *path1, const char *path2){
	if(symlink_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			symlink_file = dlsym(handle, "symlink");
		}
	} 

    int d = symlink_file(path1, path2);
    if( symlink_file != NULL) {
		fprintf(stderr, "symlink(%s, %s) = %d\n", path1, path2, d);
	}
	return d;


}

int __xstat(const char *path, struct stat *buf){
	
	if(stat_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			stat_file = dlsym(handle, "__xstat");
		}
	} 

  int d = stat_file(path, buf);
  if( stat_file != NULL) {
		fprintf(stderr, "stat(%ld, %d) = %d\n",buf->st_ino, buf->st_mode, d);
	}
	return d;
}

int __lxstat(const char *path, struct stat *buf){
	
	if(lstat_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			lstat_file = dlsym(handle, "__lxstat");
		}
	} 

	int d = lstat_file(path, buf);
	if( lstat_file != NULL) {
		fprintf(stderr, "lstat（%d)=  \n", buf->st_mode);
	}
	return d;
}

int dup(int oldfd){
	if(dup_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			dup_file = dlsym(handle, "dup");
		}
	} 

    int d = dup_file(oldfd);
    if( dup_file != NULL) {
		fprintf(stderr, "dup(%d) = %d\n", oldfd, d);
	}
	return d;
}

int dup2(int oldfd, int newfd){
	if(dup2_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			dup2_file = dlsym(handle, "dup2");
		}
	} 

    int d = dup2_file(oldfd, newfd);
    if( dup2_file != NULL) {
		fprintf(stderr, "dup2(%d, %d) = %d\n", oldfd, newfd, d);
	}
	return d;
}

int creat(const char *path, mode_t mode){
	if(creat_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			creat_file = dlsym(handle, "creat");
		}
	} 

    int d = creat_file(path, mode);
    if( creat_file != NULL) {
		fprintf(stderr, "creat(%s, %3o) = %d\n", path, mode&0777, d);
	}
	return d;
}

ssize_t pwrite(int fd, const void *buf, size_t count, off_t offset){
	if(pwrite_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			pwrite_file = dlsym(handle, "pwrite");
		}
	}

    ssize_t d = pwrite_file(fd, buf, count, offset);
    if( pwrite_file != NULL) {
		fprintf(stderr, "pwrite(%d, %p, %ld, %ld) = %ld\n", fd, buf, count, offset, d);
	}
	return d;
}