# Read me - monitor function

## Minimum requirement

## dir

    closedir
    opendir
    readdir
    mkdir
    rmdir

## file
    creat
    open
    read
    write

    dup
    dup2

    close
    lstat
    stat
    pwrite

    remove
    rename

## f -
    fopen
    fclose
    fread
    fwrite
    fgetc
    fgets
    fscanf
    fprintf
    fflush
    fclose

## ch-
    chdir chown chmod


## link-
    link unlink readlink symlink