#include <stdio.h>
#include <dlfcn.h>
#include <sys/types.h>

static int (*open_file)(const char *path, int oflag, ... ) = NULL;
static ssize_t (*read_file)(int fd, void * buf, size_t count) = NULL;
static ssize_t (*write_file)(int fd, const void *buf, size_t count) = NULL;
static int (*close_file)(int fd) = NULL;



int open(const char *path, int oflag, ... ){
    if(open_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			open_file = dlsym(handle, "open");
		}
	}

    int d = open_file(path, oflag);
    if(open_file != NULL) {
		fprintf(stderr, "open(%s, %x) = %d\n", path, oflag, d);
	}
	return d;
}

ssize_t read(int fd, void * buf, size_t count){
	if(read_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			read_file = dlsym(handle, "read");
		}
	}

    ssize_t d = read_file(fd, buf, count);
    if( read_file != NULL) {
		fprintf(stderr, "read(%d, %p, %ld) = %ld\n", fd, buf, count, d);
	}
	return d;
}

ssize_t write(int fd, const void *buf, size_t count){
	if(write_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			write_file = dlsym(handle, "write");
		}
	}

    ssize_t d = write_file(fd, buf, count);
    if( write_file != NULL) {
		fprintf(stderr, "write(%d, %p, %ld) = %ld\n", fd, buf, count, d);
	}
	return d;
}

int close(int fd){
	if(close_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			close_file = dlsym(handle, "close");
		}
	}

    int d = close_file(fd);
    if( close_file != NULL) {
		fprintf(stderr, "close(%d) = %d\n", fd, d);
	}
	return d;
}
