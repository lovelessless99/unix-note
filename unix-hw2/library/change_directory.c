#include <dlfcn.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>

static int (*chdir_dir)(const char * path) = NULL;
static int (*chown_dir)(const char *pathname, uid_t owner, gid_t group) = NULL;
static int (*chmod_dir)(const char *pathname, mode_t mode) = NULL;

int chdir(const char * path){
	if(chdir_dir == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			chdir_dir = dlsym(handle, "chdir");
		}
	} 

    int d = chdir_dir(path);
    if( chdir_dir != NULL) {
		fprintf(stderr, "chdir(%s) = %d\n", path, d);
	}
	return d;
}

int chown(const char *pathname, uid_t owner, gid_t group){
	if(chown_dir == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			chown_dir = dlsym(handle, "chown");
		}
	} 

    int d = chown_dir(pathname, owner, group);
    if( chown_dir != NULL) {
		fprintf(stderr, "chown(%s) = %d\n", pathname, d);
	}
	return d;

}

int chmod(const char *pathname, mode_t mode){
	if(chmod_dir == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			chmod_dir = dlsym(handle, "chmod");
		}
	} 

    int d = chmod_dir(pathname, mode);
    if( chmod_dir != NULL) {
		fprintf(stderr, "chmod(%s) = %d\n", pathname, d);
	}
	return d;
}