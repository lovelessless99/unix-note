#include <dlfcn.h>
#include <stdio.h>
#include <sys/types.h>

static int (*remove_file)(const char *filename) = NULL;
static int (*rename_file)(const char *old_filename, const char *new_filename) = NULL;
static int (*dup_file)(int oldfd) = NULL;
static int (*dup2_file)(int oldfd, int newfd) = NULL;
static int (*creat_file)(const char *path, mode_t mode) = NULL;


int remove(const char *filename){

	if(remove_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			remove_file = dlsym(handle, "remove");
		}
	} 

    int d = remove_file(filename);
    if( remove_file != NULL) {
		fprintf(stderr, "remove(%s) = %d\n", filename, d);
	}
	return d;
}

int rename(const char *old_filename, const char *new_filename){
	if(rename_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			rename_file = dlsym(handle, "rename");
		}
	} 

    int d = rename_file(old_filename, new_filename);
    if( rename_file != NULL) {
		fprintf(stderr, "rename(%s, %s) = %d\n", old_filename, new_filename, d);
	}
	return d;
}


int dup(int oldfd){
	if(dup_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			dup_file = dlsym(handle, "dup");
		}
	} 

    int d = dup_file(oldfd);
    if( dup_file != NULL) {
		fprintf(stderr, "dup(%d) = %d\n", oldfd, d);
	}
	return d;
}

int dup2(int oldfd, int newfd){
	if(dup2_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			dup2_file = dlsym(handle, "dup2");
		}
	} 

    int d = dup2_file(oldfd, newfd);
    if( dup2_file != NULL) {
		fprintf(stderr, "dup2(%d, %d) = %d\n", oldfd, newfd, d);
	}
	return d;
}

int creat(const char *path, mode_t mode){
	if(creat_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			creat_file = dlsym(handle, "creat");
		}
	} 

    int d = creat_file(path, mode);
    if( creat_file != NULL) {
		fprintf(stderr, "creat(%s, %3o) = %d\n", path, mode&0777, d);
	}
	return d;
}