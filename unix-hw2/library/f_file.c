#include <dlfcn.h>
#include <stdio.h>
#include <sys/types.h>


static FILE* (*fopen_file)(const char*, const char*) = NULL;
static int (*fclose_file)( FILE * stream ) = NULL;
static size_t(*fread_file)(void *ptr, size_t size, size_t nmemb, FILE *stream) = NULL;
static size_t(*fwrite_file)(const void *ptr, size_t size, size_t nmemb, FILE *stream) = NULL;


FILE* fopen(const char* filename, const char* mode){
	if(fopen_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			fopen_file = dlsym(handle, "fopen");
		}
	}

    FILE* d = fopen_file(filename, mode);
    if( fopen_file != NULL) {
		fprintf(stderr, "fopen(%s, %s) = %p\n", filename, mode, d);
	}
	return d;
}

int fclose ( FILE * stream ){
	if(fclose_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			fclose_file = dlsym(handle, "fclose");
		}
	} 

    int d = fclose_file(stream);
    if( fclose_file != NULL) {
		fprintf(stderr, "fclose(%p) = %d\n", stream, d);
	}
	return d;
}

size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream){
	if(fread_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			fread_file = dlsym(handle, "fread");
		}
	} 

    size_t d = fread_file(ptr, size, nmemb, stream);
    if( fread_file != NULL) {
		fprintf(stderr, "fread(%p, %ld, %ld, %p) = %ld\n", ptr, size, nmemb, stream, d);
	}
	return d;

}

size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream){
	if(fwrite_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			fwrite_file = dlsym(handle, "fwrite");
		}
	} 

    size_t d = fwrite_file(ptr, size, nmemb, stream);
    if( fwrite_file != NULL) {
		fprintf(stderr, "fwrite(%p, %ld, %ld, %p) = %ld\n", ptr, size, nmemb, stream, d);
	}
	return d;
}
