#include <dlfcn.h>
#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>

static int (*link_file)(const char *path1, const char *path2) = NULL;
static int (*unlink_file)(const char *pathname) = NULL;
static ssize_t (*readlink_file)(const char *pathname, char *buf, size_t bufsiz) = NULL;
static int (*symlink_file)(const char *path1, const char *path2) = NULL;


int link(const char *path1, const char *path2){
	if(link_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			link_file = dlsym(handle, "link");
		}
	} 

    int d = link_file(path1, path2);
    if( link_file != NULL) {
		fprintf(stderr, "link(%s, %s) = %d\n", path1, path2, d);
	}
	return d;
}

int unlink(const char *pathname){
	if(unlink_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			unlink_file = dlsym(handle, "unlink");
		}
	} 

    int d = unlink_file(pathname);
    if( unlink_file != NULL) {
		fprintf(stderr, "unlink(%s) = %d\n", pathname, d);
	}
	return d;
}

ssize_t readlink(const char *pathname, char *buf, size_t bufsiz){
	
	if(readlink_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			readlink_file = dlsym(handle, "readlink");
		}
	}

    ssize_t d = readlink_file(pathname, buf, bufsiz);
    if( readlink_file != NULL) {
		fprintf(stderr, "readlink(%s, %p, %ld) = %ld\n", pathname, buf, bufsiz, d);
	}
	return d;
}

int symlink(const char *path1, const char *path2){
	if(symlink_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			symlink_file = dlsym(handle, "symlink");
		}
	} 

    int d = symlink_file(path1, path2);
    if( symlink_file != NULL) {
		fprintf(stderr, "symlink(%s, %s) = %d\n", path1, path2, d);
	}
	return d;
}