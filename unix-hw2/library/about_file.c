#include <dlfcn.h>
#include <stdio.h>
#include <sys/types.h>
#include <string.h>

static int(*fget_file)(FILE * stream ) = NULL;
static char *(*fgets_file)(char *s, int size, FILE *stream) = NULL;
// static int (*fscanf_file)(FILE *stream, const char *format, ...) = NULL;
// static int (*fprintf_file)(FILE *stream, const char *format, ...) = NULL;


// static int (*stat_file)(const char *path, struct stat *buf) = NULL;
// static int (*lstat_file)(const char *path, struct stat *buf) = NULL;

static ssize_t (*pwrite_file)(int fd, const void *buf, size_t count, off_t offset) = NULL;

struct stat {
    dev_t     st_dev;     /* ID of device containing file */
    ino_t     st_ino;     /* inode number */
    mode_t    st_mode;    /* protection */
    nlink_t   st_nlink;   /* number of hard links */
    uid_t     st_uid;     /* user ID of owner */
    gid_t     st_gid;     /* group ID of owner */
    dev_t     st_rdev;    /* device ID (if special file) */
    off_t     st_size;    /* total size, in bytes */
    blksize_t st_blksize; /* blocksize for file system I/O */
    blkcnt_t  st_blocks;  /* number of 512B blocks allocated */
    time_t    st_atime;   /* time of last access */
    time_t    st_mtime;   /* time of last modification */
    time_t    st_ctime;   /* time of last status change */
};

// extern int Argc;
// __attribute__((constructor)) static void beforeFunction()
// {
//     printf("------%d-------\n", Argc);
// }

int fgetc(FILE *stream){
	if(fget_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			fget_file = dlsym(handle, "fgetc");
		}
	} 

    int d = fget_file(stream);
    if( fget_file != NULL) {
		fprintf(stderr, "fgetc(%p) = %d\n", stream, d);
	}
	return d;
}

/*some problem*/
char *fgets(char *s, int size, FILE *stream){
	if(fgets_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			fgets_file = dlsym(handle, "fgets");
		}
	} 

	char get_string[100];
	strcpy(get_string, fgets_file(s, size, stream));
    
    if( fgets_file != NULL) {
		fprintf(stderr, "fgets(%s, %d, %p) = %s\n", s, size, stream, get_string);
	}
	return get_string;
}

// int fscanf(FILE *stream, const char *format, ...){
// 	if(fscanf_file == NULL) {
// 		void *handle = dlopen("libc.so.6", RTLD_LAZY);
// 		if(handle != NULL) {
// 			fscanf_file = dlsym(handle, "fscanf");
// 		}
// 	} 
// 	va_list ap;

//     int d = fscanf_file(stream, format, va_arg(ap, int));
//     if( fscanf_file != NULL) {
// 		fprintf(stderr, "fscanf(%p, %s) = %d\n", stream, format, d);
// 	}
// 	va_end(ap);
// 	return d;
// }

// int fprintf(FILE *stream, const char *format, ...){
// 	if(fprintf_file == NULL) {
// 		void *handle = dlopen("libc.so.6", RTLD_LAZY);
// 		if(handle != NULL) {
// 			fprintf_file = dlsym(handle, "fprintf");
// 		}
// 	}

// 	va_list pl;
// 	va_start (pl, format);

// 	int d = fprintf_file(stream, format);
// 	if( fprintf_file != NULL) {
// 		vfprintf(stderr, "fprintf(%p, %s)\n", pl);
// 	}
// 	fclose(stderr);
// 	va_end(pl);
// 	return d;
// }





// extern int __xstat(const char *path, struct stat *buf){
	
// 	if(stat_file == NULL) {
// 		void *handle = dlopen("libc.so.6", RTLD_LAZY);
// 		if(handle != NULL) {
// 			stat_file = dlsym(handle, "__xstat");
// 		}
// 	} 

//   int d = stat_file(path, buf);
//   if( stat_file != NULL) {
// 		fprintf(stderr, "xstat(%s, %lu {%3o %lu}) = %d\n", path, buf->st_blksize, buf->st_mode, buf->st_size, d);
// 	}
// 	return d;
// }

// int __lxstat(const char *path, struct stat *buf){
	
// 	if(lstat_file == NULL) {
// 		void *handle = dlopen("libc.so.6", RTLD_LAZY);
// 		if(handle != NULL) {
// 			lstat_file = dlsym(handle, "__lxstat");
// 		}
// 	} 

// 	int d = lstat_file(path, buf);
// 	if( lstat_file != NULL) {
// 		fprintf(stderr, "lstat(%s, %lu {%3o %lu}) = %d\n", path, buf->st_blksize, buf->st_mode, buf->st_size, d);
// 	}
// 	return d;
// }


ssize_t pwrite(int fd, const void *buf, size_t count, off_t offset){
	if(pwrite_file == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			pwrite_file = dlsym(handle, "pwrite");
		}
	}

    ssize_t d = pwrite_file(fd, buf, count, offset);
    if( pwrite_file != NULL) {
		fprintf(stderr, "pwrite(%d, %p, %ld, %ld) = %ld\n", fd, buf, count, offset, d);
	}
	return d;
}