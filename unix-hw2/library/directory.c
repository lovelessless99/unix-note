#include <dlfcn.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>

static DIR *(*funcp)(const char *name) = NULL;
static struct dirent *(*rdir)(DIR *) = NULL;
static int (*cdir)(DIR *) = NULL;
static int (*mkdir_dir)(const char *path, mode_t mode) = NULL;
static int (*rmdir_dir)(const char *path) = NULL;

DIR* opendir(const char *name) {
	if(funcp == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			funcp = dlsym(handle, "opendir");
		}
	}

	DIR * d = funcp(name);
	if(funcp != NULL) {
		fprintf(stderr, "opendir(%s) = %p\n", name, d);
	}
	return d;
}


struct dirent * readdir(DIR * dirp) {
	
	if(rdir == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			rdir = dlsym(handle, "readdir");
		}
	}
	
	struct dirent *d;
	if( (d = rdir(dirp)) != NULL) {
		fprintf(stderr, "readdir = %s\n" , d->d_name);
	}
	return d;
}

int closedir(DIR *dirp){
	if(cdir == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			cdir = dlsym(handle, "closedir");
		}
	}
	
	int d = cdir(dirp);
	if(cdir != NULL) {
		fprintf(stderr, "closedir(%p) = %d\n" ,dirp , d);
	}
	return d;
}

int mkdir(const char *path, mode_t mode){
	if(mkdir_dir == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			mkdir_dir = dlsym(handle, "mkdir");
		}
	} 

    int d = mkdir_dir(path, mode);
    if( mkdir_dir != NULL) {
		fprintf(stderr, "mkdir(%s, %3o) = %d\n", path, mode&0777, d);
	}
	return d;
}

int rmdir(const char *path){
	if(rmdir_dir == NULL) {
		void *handle = dlopen("libc.so.6", RTLD_LAZY);
		if(handle != NULL) {
			rmdir_dir = dlsym(handle, "rmdir");
		}
	} 

    int d = rmdir_dir(path);
    if( rmdir_dir != NULL) {
		fprintf(stderr, "rmdir(%s) = %d\n", path, d);
	}
	return d;
}
